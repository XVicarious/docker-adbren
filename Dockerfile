FROM alpine:3.9
MAINTAINER Brian Maurer aka XVicarious
RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories
WORKDIR /opt
RUN apk --no-cache upgrade &&\
    apk add --no-cache perl wget build-base perl-dev git perl-app-cpanminus &&\
    cpanm --install File::HomeDir File::Pid Digest::MD4 &&\
    git clone --single-branch --branch master --depth 1 https://github.com/clip9/adbren &&\
    rm -rf /opt/adbren/.git &&\
    sed -i -e "s/\$self->_get_pid_from_file;/\$self->_get_pid_from_file or return;/g" /usr/local/share/perl5/site_perl/File/Pid.pm &&\
    apk del wget build-base perl-dev git perl-app-cpanminus &&\
    ln -s /opt/adbren/adbren.pl /usr/local/bin/
RUN rm -rf /root/.cpanm
VOLUME /root/adbren
VOLUME /media
WORKDIR /media
ENTRYPOINT [ "/usr/local/bin/adbren.pl" ]
