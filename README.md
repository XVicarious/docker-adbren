## docker-adbren
### Use [adbren (AniDB Renamer)](https://github.com/clip9/adbren) with ease!

This docker container simply contains adbren and its deps. This also fixes the issue with `File::Pid`.

Included is a shell script "adbren". I suggest linking it to something like `~/.local/bin/` which makes it a breeze to call.
